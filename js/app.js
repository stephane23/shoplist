new Vue({
    el: '#app',
    data: {
        panier: [
            
        ],
        input: { 
            product: '', 
            quantity: 0, 
            price: 0 ,
            selected: false, 
        },
        countItems: 0,
        budget: ''
    },
    computed: {
        validInput: function() {
            return this.input.product!='' && this.input.quantity!='' && this.input.price!='';
        },
        total: function() {
            var total = 0;
            this.panier.forEach(function(el) {
            total += (el.price * el.quantity);
            });
            return total.toFixed(2); 
        },
        totalQuantity: function() {
            var total = 0;
            this.panier.forEach(function(el) {
            total += parseInt(el.quantity, 10);
            });
            return total; 
        },
        countItem: function() {
            return this.countItems = this.panier.length;
        },
        verifyCart: function() {
            let selectedItemsCount = this.panier.filter(function (item) {
            return item.selected
            }).length;
            if (this.panier.length > 0) {
                return selectedItemsCount === this.panier.length;
            }
        },
    },
    methods: {
        add: function() {
            this.panier.push(this.input);
            this.input = { product: '', quantity: 0, price: 0, selected: false};
        },
        deleteInput: function(index) {
            this.panier.splice(index, 1);
        },
        deleteAll: function(index) {
            this.panier.splice(this.index);
            this.verifyCart = false;
        },
    },
  });

